<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $quarded = [];
    
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
